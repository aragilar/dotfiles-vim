" Minimal vimrc
set nocompatible
" Load plugins
call pathogen#infect('plugins-enabled/{}')

source ~/.vim/functions.vim
source ~/.vim/aliases.vim
source ~/.vim/keybindings.vim
source ~/.vim/behaviour.vim
source ~/.vim/interaction.vim
source ~/.vim/display.vim
