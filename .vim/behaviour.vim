" Disable beeping
set vb t_vb=
" Load indentation rules and plugins according to the detected filetype.
filetype plugin indent on

" Pay attention to modelines in files.
set modeline

" Create backup files.
set nobackup

" Remember previously used commands.
set history=100

" Default indentation settings
" NOTE: these may be overridden for specific filetypes

" Default text options:
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set textwidth=80
set colorcolumn=+1

" Automatically continue indentation across lines
" Keep indentation structure (tabs, spaces, etc) when autoindenting.
" Automatically indent new lines after specific keywords or braces
set autoindent
set copyindent

" Omnicomplete
set ofu=syntaxcomplete#Complete
set completeopt=longest,menuone,preview
"
" Spell-checking
set spell spelllang=en_au

let g:tex_flavor = "latex"
let g:LatexBox_Folding = "yes"

" Syntastic settings
let g:syntastic_check_on_open=1
"let g:syntastic_auto_jump=1
let g:syntastic_auto_loc_list=1

" Hardtime settings
let g:hardtime_default_on = 1
let g:hardtime_ignore_buffer_patterns = ["-MiniBufExplorer-"]

" use ack for grep
set grepprg=ack\ -k

" use zip plugin on other extensions
autocmd BufReadCmd *.whl call zip#Browse(expand("<amatch>"))
