" map <up> <nop>
" map <down> <nop>
" map <left> <nop>
" map <right> <nop>

"let mapleader = ","

noremap <silent> <Space> :silent noh<Bar>echo<CR>

cmap w!! w !sudo tee % >/dev/null

set pastetoggle=<F2>

map <F4> :TagbarOpenAutoClose<CR>
map <F8> :!/usr/bin/ctags -R --c++-kinds=+pl --fields=+iaS --extra=+q .<CR>

nnoremap <silent> <Leader>ml :call AppendModeline()<CR>

"nnoremap <leader>r :RainbowParenthesesToggle<cr>
