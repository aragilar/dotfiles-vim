setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2

" Maths Mappings
inoremap (( \left(\right)<esc>7ha
inoremap [[ \left[\right]<esc>7ha
"inoremap || \left|\right|<esc>7ha  - wasn't working ?
inoremap {{ \left\{\right\}<esc>8ha

inoremap __ _{}<esc>i
inoremap ^^ ^{}<esc>i

inoremap $$ $$<esc>i
