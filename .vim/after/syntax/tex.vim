" This seemed to depend on latex-box, commented out for now
"" Add amsmath envs
"call TexNewMathZone("E","align",1)
"call TexNewMathZone("F","gather",0)
"
"" Highlight biblatex
"syn region texRefZone		matchgroup=texStatement start="\\textcite{"		end="}\|%stopzone\>"	contains=@texRefGroup
"syn region texRefZone		matchgroup=texStatement start="\\parencite{"		end="}\|%stopzone\>"	contains=@texRefGroup
"
